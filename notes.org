* Configuration
Copy twitter4j.properties_example to twitter4j.properties and fill in the properties with your twitter developer keys (you can generate the keys for free at https://developer.twitter.com/)

* Compilation
Run make.

* Running
Run ./twitterlisp.sh username
where username is the screen name of the account associated with the developer keys.
(you need to run chmod +x twitterlisp.sh if it's not executable)

* Notes
The main source file is twitterlisp.java

You need the twitter4j library. A version that works as of 12/28/2020 is version 4.0.7. It is included for convenience.

It also uses the AJL java lisp library, a version of which is included.

If you are having trouble getting the program to respond to your tweets, try disabling the "Protect your Tweets" feature in the "Audience and tagging settings" for the account you are using to contact the bot.

For other issues, try setting debug=true in the twitter4j.properties file.
