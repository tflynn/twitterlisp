/*
 * Copyright 2016, 2020 Thomas Flynn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import twitter4j.*;
import java.util.Date;
import twitter4j.conf.ConfigurationBuilder;

import org.ajl.Env;
import org.ajl.fn_vals;
import org.ajl.lisp;
import org.ajl.sym_vals;


class simpleStreamListener
	extends dumbStreamListener {
	
	Twitter twitter =  new TwitterFactory().getInstance();
	 	
	@Override
	public void onStatus(Status status) {
		if(status.getUser().getScreenName().equals("twtscm"))
			return;
		
		System.out.println("From: "
											 + status.getUser().getScreenName()
											 + " Contents: "
											 + status.getText());

		String the_msg = status.getText();
		String the_msg_stripped;
		
		if(the_msg.substring(0,7).equals("@" + screenname))
			the_msg_stripped=the_msg.substring(7);
		else
			the_msg_stripped=the_msg;
		
		String response_content =
			handle_message_simple(the_msg_stripped);
		
		String response_full = "@"
			+ status.getUser().getScreenName()
			+ " " + 
			response_content;

		if(response_full.length() > 140)
			response_full = response_full.substring(0,136) + " ...";

		System.out.println("Replying to"
											 + status.getUser().getScreenName()
											 + " with "
											 + response_full);
			
		twitterlisp.tweet(response_full,
											twitter,
											status.getId());
	}
	
	Env env = lisp.standard_env();
	String res;
	
	public String screenname;
	
	public String handle_message_simple(String txt){
		res="";
		
		String finalres = lisp.runProg_nothrow(txt,env);
		return res + finalres;
	}
	
	//	public abstract String handle_message_simple(String txt);
}

public class twitterlisp {
	
	static void tweet(String the_tweet,
										Twitter twitter,
										Long inReplyToStatusId){
		try {
	    System.out.println("Updating status to " + the_tweet);
	    StatusUpdate statusUpdate
				= new StatusUpdate(the_tweet);
	    System.out.println("In reply to " + inReplyToStatusId);
	    statusUpdate.setInReplyToStatusId(inReplyToStatusId);
	    
	    twitter.updateStatus(statusUpdate);
	    
	  }catch (TwitterException te){
			System.out.println("Exception:");
			System.out.println(te.toString());
	  }
		
	}
	
	public static final simpleStreamListener listener =		new simpleStreamListener();
	public static void main(String[] args)
		throws TwitterException {
		
		if (args.length < 1){
			System.out.println("Error: Missing screen name argument");
			return;
		}
		
		TwitterStream twitterStream
			= new TwitterStreamFactory().getInstance();
		listener.screenname = args[0];
		twitterStream.addListener(listener);

		twitterStream.filter("@" + listener.screenname);
	}
	
  

}
